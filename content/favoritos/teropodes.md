---
title: "Theropoda Favorito"
date: 2021-10-18T20:59:46-03:00
image: /images/t_rex.jpg
draft: false
---

O favorito dessa categoria é possivelmente o mais icônico dos dinossaruos: o Tiranossauro Rex.

Tyrannosaurus é um gênero de dinossauros terópodes celurossauros que viveram durante o final do período cretáceo, há aproximadamente 66 milhões de anos, em toda a região que hoje é a América do Norte. O único representante do gênero é Tyrannosaurus rex, que ganhou o epíteto específico de rex, por ser o maior dinossauro carnívoro conhecido quando foi descoberto.

Em 2004, foi proposta a hipótese de que os tiranossauros poderiam ser animais providos de penas, ideia formulada após serem achados vestígios de penas em estado primitivo em alguns tiranossaurídeos como o Dilong e o Yutyrannus. Existe ainda uma discussão sobre em que estágio do desenvolvimento as penas deveriam estar. Os achados fósseis sugerem que as penas nos Tirannosaurídeos deveriam estar no 1° estágio o que indica plumas similar aquelas encontradas em pintinhos, o que daria uma aparência similar a de pelos.Todavia, essa teoria foi recebida com descrença por parte da comunidade científica, pois tanto na Ásia quanto no América do Norte foram achados vestígios epiteliais de alguns tiranossaurídeos apresentando abundância de escamas, típico dos dinossauros.

Basicamente, não há consenso na comunidade científica sobre a presença de penas recobrindo o Tiranossauro Rex, mas optou-se por usar uma imagem em que ele apresenta penas porque esse conceito estético é mais interessante!
