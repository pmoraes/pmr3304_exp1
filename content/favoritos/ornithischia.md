---
title: "Ornithischia Favorito"
date: 2021-10-18T21:06:01-03:00
image: /images/iguanodon.jpg
draft: false
---

Nessa categoria temos como favorito o Iguanodon, cuja espécie protagonizou o clássico da animação "Dinossauro (2000)", produzido pela Walt Disney Feature Animation e distribuído pela Walt Disney Pictures.

O iguanodon, do latim "dente de iguana", também conhecido como iguanodonte ou iguanodon, é um gênero de dinossauro herbívoro e bípede que viveu no início do período Cretáceo Inferior. Media em torno de 9 metros de comprimento, pesava cerca de 4, 5 toneladas e pensa-se que pudessem correr a 31  km/h. Conhecem-se vestígios de Iguanodon do Reino Unido, França, Bélgica, Portugal, Estados Unidos, Brasil, República Checa e Macedônia do Norte.

As características extraordinárias da mão são o que fazem este dinossauro ornitópode tão especial. As articulações dos três dedos médios permitiam-lhe dobrá-los posteriormente e as pontas teriam uma espécie de cascos. Em cada dedo polegar, o Iguanodon possuía um esporão afiado, que provavelmente era utilizado para defesa contra dinossauros carnívoros. Já o quinto dedo era reduzido e poderia ser usado para agarrar e segurar os alimentos.