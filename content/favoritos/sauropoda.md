---
title: "Sauropoda Favorito"
date: 2021-10-18T20:59:46-03:00
image: /images/brachiosaurus.jpg
draft: false
---

O favorito na categoria Sauropoda é o enorme Braquissauro.

O Braquiossauro, cujo nome significa "lagarto braço", dado os seus membros anteriores ("braços") serem maiores que os posteriores, era um gênero de dinossauros saurópode que viveu durante o fim do período Jurássico. Este gigante tinha entre 18 e 20 metros de altura e cerca de 25 metros de comprimento. Estima-se que o peso do animal girava em torno de 50 toneladas. O primeiro braquiossauro foi descoberto em 1900 no Colorado, Estados Unidos, mas também viveu na área onde se localiza hoje a Argélia e a Tunísia, há aproximadamente 144 milhões de anos, durante o período Jurássico. Esse animal provavelmente não poderia erguer-se nas patas traseiras como mostra o filme "Jurassic Park", pois elas eram mais curtas que as dianteiras. Mesmo assim sua altura lhe permitia, sem esforço, comer as copas das árvores, sua atividade principal.